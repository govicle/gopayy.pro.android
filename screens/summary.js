import { View, Text, Image, FlatList, ScrollView, SectionList, StyleSheet, TouchableOpacity, Modal } from "react-native"
import Button from "../components/button"
import { useEffect, useState } from "react";
import { color } from "../style/color"
import styles from "../style/style"
import Card from "../components/card"
import Header from "../components/header"
import AuthContext from "../util/AuthContext"
import { API, getToken, removeToken } from "../util/services"
import React from "react"
import { CheckBox, Icon } from "react-native-elements"
import AccordionView from "../components/accordion";
import Collapsible from "react-native-collapsible";
import Loading from "../components/loading";
import { useIsFocused } from "@react-navigation/native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import moment from "moment";
import paymentChannels from "../util/modelClass/paymentChannel";
import { RefreshControl } from "react-native-gesture-handler";


const Summary = ({ route, navigation }) => {
    const { item } = route.params
    const isFocused = useIsFocused()
    const [collectionBy, setCollectionBy] = useState(`monthly`)
    const [channels, setChannels] = useState([])
    const [channelList, setChannelList] = useState([])
    const [triggerChanelList, setTriggerChannelList] = useState(false)
    const [loading, setLoading] = useState(false)
    const [organizationId, setOrganizationId] = useState()
    const [relist, setRelist] = useState(false)
    const [collections, setCollections] = useState([])
    const [datePickerMode, setDatePickerMode] = useState(`datepicker`)
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [isYearPickerVisible, setIsYearPickerVisible] = useState(false);
    const [isFilterVisible, setIsFilterVisible] = useState(false);
    const [isCollectionByPickerVisible, setIsCollectionByPickerVisible] = useState(false);
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState()
    const [toDateStart, setToDateStart] = useState(true)
    const currentYear = new Date().getFullYear();




    const getYearList = (startYear, endYear) => {
        const yearList = [];
        for (let year = startYear; year <= endYear; year++) {
            yearList.push(year);
        }
        return yearList;
    };

    function getEndOfMonth(date) {
        const nextMonth = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        const endOfMonth = new Date(nextMonth - (24 * 60 * 60 * 1000));
        // console.log(endOfMonth)
        return endOfMonth;
    }



    useEffect(() => {
        if (isFocused) {
            setLoading(true)
            getSummary(item, collectionBy, channels, startDate)
            if (channelList.length == 0) {
                getChannelList()
            }

            console.log(item)

        }

    }, [isFocused])
    async function getSummary(item, collection_by, payment_channels, date_start, date_end) {
        getToken().then(token => {
            var merchantId = ``
            var productId = ``
            var staffId = ``
            var orgId =``
            switch (item.type) {
                case `merchant`: {
                    merchantId = item.id
                    break
                }
                case `staff`: {
                    staffId = item.id
                    break
                }
                case `product`: {
                    productId = item.id
                    break
                }
                case `organization`: {
                    orgId = item.id
                    break
                }
                default: {
                    break
                }
            }
            API.getSummary(token, collection_by, payment_channels, orgId, merchantId, staffId, productId, moment(date_start).format(`YYYY-MM-DD`), date_end ? moment(date_end).format(`YYYY-MM-DD`) : null)
                .then(res => {
                    setLoading(false)
                    setCollections(res.data.collections)
                    setupIsCollapse(res.data.collections)
                    setRelist(!relist)
                    // console.log(JSON.stringify(res))
                    // setJoblist(res.joblists)
                    // setUser(res.user)

                })
                .catch(err => {
                    setLoading(false)
                    alert(err.error ? err.error : err);
                    // console.log(err.message)
                    // setLoadingProfile(false);
                })
        })
    }

    async function getChannelList() {
        let newList = []
        paymentChannels.getChannels().forEach(item => {
            var newObj = {
                id: item.id,
                code: item.code,
                name: item.name,
                selected: true,
            }
            newList.push(newObj)
        })
        updateChannel(newList)
        setChannelList(newList)
    }

    function selectChannel(item, index) {
        let obj = channelList
        // console.log(index)
        obj[index].selected = !obj[index].selected
        // console.log(obj[index])
        // updateChannel(obj)
        setChannelList(obj)
        setTriggerChannelList(!triggerChanelList)
    }
    function updateChannel(list) {
        var newListobj = []
        list.forEach(item => {
            if (item.selected == true) {
                newListobj.push(item.code)
            }
        });
        // console.log(newListobj)
        setChannels(newListobj)
        setLoading(true)
        getSummary(item, collectionBy, newListobj, startDate, endDate)

    }

    const showDatePicker = () => {
        switch (collectionBy) {
            case `yearly`:
                setIsYearPickerVisible(true);
                break
            case 'monthly':
                setDatePickerMode(`monthYear`)
                setIsDatePickerVisible(true);
                break
            default:
                setDatePickerMode(`date`)
                setIsDatePickerVisible(true);
                break
        }

    }
    const hideDatePicker = () => {
        switch (collectionBy) {
            case `yearly`:
                setIsYearPickerVisible(false);
                break
            case 'monthly':
                setIsDatePickerVisible(false);
                break
            default:
                setIsDatePickerVisible(false);
                break
        }
    }
    const handleDatePick = (date) => {
        // setLogDate(date);
        if (toDateStart) {
            setStartDate(date)
            if (collectionBy == `daterange`) {
                setLoading(true)
                getSummary(item, collectionBy, channels, date, endDate)
            } else {
                setLoading(true)
                getSummary(item, collectionBy, channels, date)
            }


        } else {
            setLoading(true)
            setEndDate(date)
            getSummary(item, collectionBy, channels, startDate, date)
        }
        hideDatePicker();

    }

    const handleCollectionByPick = (selected) => {
        switch (selected) {
            case `daterange`:
                setLoading(true)
                setEndDate(getEndOfMonth(startDate))
                setIsCollectionByPickerVisible(false)
                setCollectionBy(selected)
                getSummary(item, selected, channels, startDate, getEndOfMonth(startDate))
                break
            default:
                setLoading(true)
                setIsCollectionByPickerVisible(false)
                setCollectionBy(selected)
                getSummary(item, selected, channels, startDate)
                break
        }

    }
    const handlePress = (selected) => {
        var selectedStartDate = (collectionBy == `yearly`) ? `${startDate.getFullYear()}-${selected.date_start}-01` : selected.date_start
        var selectedEndDate = (collectionBy == `yearly`) ? moment(getEndOfMonth(new Date(selectedStartDate))).format(`YYYY-MM-DD`) : selectedStartDate
        navigation.navigate(`Log`, {
            filters: {
                user: {
                    id: item.id,
                    type: item.type
                },
                date_start: selectedStartDate,
                date_end: selectedEndDate,
                collection_by: collectionBy,
                channels: channels
            }

        })
        // console.log(`Pressed ${item.title}`);
        // Add your logic here
    };

    const handlePressChannel = (channel, date_start) => {
        var selectedStartDate = (collectionBy == `yearly`) ? `${startDate.getFullYear()}-${date_start}-01` : date_start
        var selectedEndDate = (collectionBy == `yearly`) ? moment(getEndOfMonth(new Date(selectedStartDate))).format(`YYYY-MM-DD`) : selectedStartDate
        navigation.navigate(`Log`, {
            filters: {
                user: {
                    id: item.id,
                    type: item.type
                },
                date_start: selectedStartDate,
                date_end: selectedEndDate,
                collection_by: collectionBy,
                channels: channel.code
            }

        })
        // Add your logic here
    };
    function channelLogo(code) {
        const channelLogos = {
            'cash': require('../resource/channels_logo/cash.png'),
            'fasspay_card': require('../resource/channels_logo/fasspay_card.png'),
            'fasspay_qr': require('../resource/channels_logo/fasspay_qr.png'),
            // 'counter': require('../resource/channels_logo/counter.png'),
            'billplz': require('../resource/channels_logo/billplz.png'),
            'record': require('../resource/channels_logo/record.png'),
            // 'invoice': require('../resource/channels_logo/invoice.png'),
            'grab': require('../resource/channels_logo/grab.png'),
            'foodpanda': require('../resource/channels_logo/foodpanda.png'),
            'shopee': require('../resource/channels_logo/shopee.png'),
            'others': require('../resource/channels_logo/others.png')
        };

        return channelLogos[code] || channelLogos['others'];

    }

    const [isCollapsed, setIsCollapse] = useState([])

    const ChannelItem = ({ item, date_start }) => {
            return (

                <View style={{ alignItems: `center`, width: `33%` }}>
                    <View style={{ alignItems: `center`, justifyContent: `center`, borderRadius: 14, paddingHorizontal: 4, paddingVertical: 4, width: `100%`, }}>
                        <TouchableOpacity
                            style={{ justifyContent: `space-around`, alignItems: `center`, borderRadius: 5, width: '100%', height: 40, flexDirection: `row`, padding: 4 }}
                            onPress={() => { handlePressChannel(item, date_start) }}>
                            <View style={{ justifyContent: `center`, alignItems: `center`, flexDirection: `row`, width: `30%` }}>
                                <View style={{ width: `100%`, borderRadius: 14, overflow: 'hidden' }}>
                                    <Image style={{ width: `100%`, height: `100%`, resizeMode: `contain` }} source={channelLogo(item.code)} />
                                </View>

                            </View>
                            <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, height: 20, width: `70%` }}>
                                <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, flexDirection: `row` }}>
                                    <Text style={{ color: color.primaryText, fontSize: 8 }}>RM</Text>
                                    <Text style={{ color: color.primaryText, fontWeight: `600` }}>{(item.total) ? (parseFloat(item.total) > 1000) ? `${(parseFloat(item.total) / 1000).toFixed(1)}K~` : parseFloat(item.total).toFixed(2) : `0.00`}</Text>
                                </View>
                                {(parseFloat(item.total) > 1000) &&
                                    <Text style={{ paddingBottom: 2, color: color.primaryText, fontSize: 8 }}>({parseFloat(item.total).toFixed(2)})</Text>
                                }
                            </View>
                        </TouchableOpacity>

                        {/* <Text style={styles.text}>{item.name}</Text> */}
                    </View>
                </View>

            )
    }
    function getDateString(date, type) {
        switch (collectionBy) {
            case `yearly`:
                return date.toLocaleString("en-MY", { year: `numeric` });
            case 'monthly':
                return date.toLocaleString("en-MY", { year: `numeric`, month: `long` });
            default:
                return date.toLocaleString("en-MY", { year: `numeric`, month: `short`, day: `2-digit` });
        }

    }
    function setupIsCollapse(list) {
        let obj = isCollapsed;
        list.map(item => {
            obj.push(true);
        })
        setIsCollapse(obj)

    }
    const collapsiblePress = (index) => {
        let obj = isCollapsed
        // console.log(obj[index])
        obj[index] = !obj[index]
        // console.log(obj[index])
        setIsCollapse(obj)
        setRelist(!relist)
    }

    const SummaryItem = ({ summary, index }) => {
        return (

            <View>
                <Card onPress={() => handlePress({ date_start: summary.sequence })} >
                    <View style={{ alignContent: `space-between` }}>
                        <View style={{ flexDirection: `row`, flexWrap: `wrap`, alignContent: `space-between`, marginVertical: 8, alignItems: "center" }}>
                            <View style={{ paddingStart: 6, paddingEnd: 8, flex: 1, justifyContent: `center` }}>
                                <Text style={styles.HeaderTextMain}>{summary.title}</Text>
                            </View>
                            <View style={{ paddingStart: 6, paddingEnd: 8, alignContent: `center` }}>
                                <Text style={[styles.HeaderTextBlack, { fontSize: 18 }]}>RM{parseFloat(summary.tx_total).toFixed(2)}</Text>
                            </View>

                            <TouchableOpacity
                                hitSlop={24}
                                onPress={() => collapsiblePress(index)}>
                                <View style={{ justifyContent: `center` }}>
                                    <Icon type="font-awesome"
                                        name={isCollapsed[index] ? "chevron-circle-down" : "chevron-circle-up"}
                                        size={22}
                                        color={color.primary} />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Collapsible collapsed={isCollapsed[index]}>
                                <View style={{ backgroundColor: color.secondary, borderBottomRightRadius: 14, borderBottomLeftRadius: 14, justifyContent: 'center', marginHorizontal: -16, marginBottom: -16, paddingHorizontal: 16 }}>
                                    <FlatList
                                        data={summary.channels.filter(channel => channel.total > 0)}
                                        keyExtractor={(item, index) => index}
                                        numColumns={3}
                                        renderItem={({ item }) => < ChannelItem item={item} date_start={summary.sequence} />}
                                    />

                                </View>



                            </Collapsible>
                        </View>


                    </View>
                </Card>

            </View>

        );
    };


    return (
        <View style={{ flex: 1 }}>
            {/* <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="year"
                display="inline"
                onConfirm={handleDatePick}
                onCancel={hideDatePicker}
            /> */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={isDatePickerVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setModalVisible(!attachmentModal);
                }}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPressOut={() => { setIsDatePickerVisible(false) }}
                    style={{ flex: 1, margin: 0, justifyContent: 'flex-end' }}>
                    <DatePicker
                        mode={datePickerMode ? datePickerMode : `datepicker`}
                        selectorStartingYear={2023}
                        onMonthYearChange={selectedDate => handleDatePick(new Date(selectedDate.split(` `).map((n, index, array) => index === 0 || index === array.length - 1 ? n : "").join("-")))}
                        onSelectedChange={date => handleDatePick(new Date(moment(date, `YYYY/MM/DD`)))}
                    />
                </TouchableOpacity>

            </Modal>
            <Modal
                animationType='slide'
                transparent={true}
                visible={isYearPickerVisible}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPressOut={() => { setIsYearPickerVisible(false) }}
                    style={{ flex: 1, margin: 0, justifyContent: 'flex-end' }}>
                    <View style={{ paddingHorizontal: 8 }}>
                        {/* <TouchableWithoutFeedback> */}
                        <View style={{ borderRadius: 24, backgroundColor: `white`, paddingBottom: 36, paddingTop: 16, marginBottom: -20, alignItems: "center", minHeight: 100, maxHeight: 300 }}>

                            <FlatList
                                data={getYearList(2023, currentYear)}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => index}
                                renderItem={({ item }) => {
                                    return (
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => { handleDatePick(new Date(`${item}-01-01`)) }}>
                                                <View style={{ padding: 8, justifyContent: `center`, alignItems: `center`, borderBottomWidth: 0.4, marginBottom: 16 }}>
                                                    <Text style={[styles.HeaderTextBlack, { fontSize: 24 }]}>
                                                        {item}
                                                    </Text>
                                                </View>

                                            </TouchableOpacity>

                                        </View>)
                                }}
                            />

                        </View>
                        {/* </TouchableWithoutFeedback> */}
                    </View>
                </TouchableOpacity >
            </Modal>

            <Modal
                animationType='slide'
                transparent={true}
                visible={isCollectionByPickerVisible}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPressOut={() => { setIsCollectionByPickerVisible(false) }}
                    style={{ flex: 1, margin: 0, justifyContent: 'flex-end' }}>
                    <View style={{ paddingHorizontal: 8 }}>
                        {/* <TouchableWithoutFeedback> */}
                        <View style={{ borderRadius: 24, backgroundColor: `white`, paddingBottom: 36, paddingTop: 16, marginBottom: -20, alignItems: "center", minHeight: 100, maxHeight: 300 }}>

                            <FlatList
                                data={[`yearly`, `monthly`, `daterange`]}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => index}
                                renderItem={({ item }) => {
                                    return (
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => handleCollectionByPick(item)}>
                                                <View style={{ padding: 8, justifyContent: `center`, alignItems: `center`, borderBottomWidth: 0.4, marginBottom: 16 }}>
                                                    <Text style={[styles.HeaderTextBlack, { fontSize: 24 }]}>
                                                        {item.charAt(0).toUpperCase() + item.slice(1)}
                                                    </Text>
                                                </View>

                                            </TouchableOpacity>

                                        </View>)
                                }}
                            />

                        </View>
                        {/* </TouchableWithoutFeedback> */}
                    </View>
                </TouchableOpacity >
            </Modal>

            <Modal
                animationType='slide'
                transparent={true}
                visible={isFilterVisible}>
                <TouchableOpacity
                    activeOpacity={1}
                    onPressOut={() => {
                        setIsFilterVisible(false)
                        updateChannel(channelList)
                    }}
                    style={{ flex: 1, margin: 0, justifyContent: 'flex-end' }}>
                    <View style={{ paddingHorizontal: 8 }}>

                        {/* <TouchableWithoutFeedback> */}
                        <View style={{ borderRadius: 24, backgroundColor: `white`, paddingBottom: 60, paddingTop: 8, marginBottom: -20, alignItems: "center", justifyContent: `center`, minHeight: 100, paddingHorizontal: 16 }}>
                            <View>
                                <Text style={[styles.HeaderTextBlack, { fontSize: 20, paddingVertical: 16 }]}>Payment Channel</Text>
                            </View>
                            <FlatList
                                data={channelList}
                                extraData={triggerChanelList}
                                showsVerticalScrollIndicator={false}
                                numColumns={2}
                                keyExtractor={(item, index) => index}
                                columnWrapperStyle={{ flex: 1, justifyContent: `space-between` }}
                                renderItem={({ item, index }) => {
                                    return (
                                        <View>

                                            <View style={{ width: 160, borderRadius: 14, margin: 8, padding: 8,paddingVertical:16 }}>
                                                <View>

                                                    <TouchableOpacity 
                                                    onPress={() => selectChannel(item, index)}
                                                    style={{ flexDirection: `row`,alignItems:`center`}}>
                                                   
                                                        <View style={{ width: 20, height: 20, backgroundColor: (!item.selected) ? color.secondary : color.primarySoft, borderRadius: 5,borderWidth:1,borderColor:`#ABAFD0`,alignItems:`center`,justifyContent:`center`}}>
                                                        <Icon type="font-awsome" name="check" size={17} color={color.secondary} />
                                                        </View>
                                                           
                                                        <Text style={[styles.HeaderTextBlack, { fontSize: 14, fontWeight: `bold`,paddingHorizontal:8}]}>
                                                        {item.name}
                                                    </Text>
                                                    </TouchableOpacity>
                                                    
                                                </View>





                                            </View>

                                        </View>)
                                }}
                            />

                        </View>
                        {/* </TouchableWithoutFeedback> */}
                    </View>
                </TouchableOpacity >
            </Modal>

            <Header
                title={`${item.title} Summary`}
                onBackPress={() => {
                    navigation.goBack()
                }}
            />
            {
                (loading) ?
                    <Loading />
                    :
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <FlatList
                            onRefresh={()=>{   
                                setLoading(true)
                                getSummary(item, collectionBy, channels, startDate)}
                            }
                            refreshing = {loading}
                                ListHeaderComponent={<View>
                                    <View style={{ flexDirection: `row` }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                setIsCollectionByPickerVisible(true)
                                            }}
                                            style={{ flexDirection: `row`, flex: 1, marginVertical: 8, marginHorizontal: 16, borderRadius: 10, backgroundColor: `white`, padding: 16 }}>
                                            <Text style={[styles.HeaderTextBlack, { justifyContent: `center`, flex: 1 }]}>{collectionBy.charAt(0).toUpperCase() + collectionBy.slice(1)}</Text>
                                            <View style={{ justifyContent: `center` }}>
                                                <Icon type="antdesign"
                                                    name={"down"}
                                                    size={20}
                                                    color={color.primary} />
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => { setIsFilterVisible(true) }}
                                            style={{ marginVertical: 8, marginEnd: 16, borderRadius: 10, backgroundColor: `white`, padding: 16 }}>
                                            <View style={{ justifyContent: `center` }}>
                                                <Icon type="antdesign"
                                                    name={"filter"}
                                                    size={20}
                                                    color={color.primary} />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flexDirection: `row`, marginVertical: 8, marginHorizontal: 16, alignItems: `center` }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                setToDateStart(true)
                                                showDatePicker()
                                            }}
                                            style={{ flex: 1, marginVertical: 8, marginEnd: 8, borderRadius: 10, backgroundColor: `white`, alignItems: `center`, justifyContent: `center`, paddingHorizontal: 8, paddingVertical: 16 }}>
                                            <Text style={[styles.HeaderTextBlack, { fontSize: 18 }]}>{`${getDateString(startDate)}`}</Text>
                                        </TouchableOpacity>
                                        {
                                            (collectionBy == `daterange`) &&
                                            <>
                                                <Text style={styles.HeaderTextBlack}>-</Text>

                                                <TouchableOpacity
                                                    onPress={() => {
                                                        setToDateStart(false)
                                                        showDatePicker()

                                                    }}
                                                    style={{ flex: 1, marginVertical: 8, marginStart: 8, borderRadius: 10, backgroundColor: `white`, alignItems: `center`, justifyContent: `center`, paddingHorizontal: 8, paddingVertical: 16 }}>
                                                    <Text style={[styles.HeaderTextBlack]}>{`${getDateString(endDate)}`}</Text>
                                                </TouchableOpacity>
                                            </>
                                        }



                                    </View>
                                    {
                                        (collections.length == 0) &&
                                        <View style={{ flex: 1, alignItems: `center`, justifyContent: `center` }}>
                                            <Image style={{ resizeMode: 'contain' }} source={require(`../resource/empty.png`)} />
                                        </View>
                                    }

                                </View>}
                                extraData={relist}
                                data={collections}
                                keyExtractor={(item, index) => index}
                                renderItem={({ item, index }) => < SummaryItem summary={item} index={index} />}
                            />
                        </View>


                    </View>



            }

        </View>


    )


}
export default Summary