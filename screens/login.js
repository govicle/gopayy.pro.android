import { View, SafeAreaView, Image, Text, TouchableOpacity, KeyboardAvoidingView, ScrollView, TouchableWithoutFeedback, Keyboard, Platform } from "react-native";
import { useEffect, useState } from "react";
import { color } from "../style/color";
import styles from "../style/style";
import { Input } from "react-native-elements";
import Button from "../components/button";
import AuthContext from "../util/AuthContext";
import React from "react";
import { Icon } from "react-native-elements"
import IconButton from "../components/iconButton";
import { API } from "../util/services";

const Login = () => {

    const [login, setLogin] = useState({ username: ``, password: `` })
    const [errors, setErrors] = useState({});
    const [showPassword, setShow] = useState(false)
    const [passwordIcon, setIcon] = useState("eye-with-line")
    const { signIn } = React.useContext(AuthContext)

    const toggleShowPassword = () => {
        setShow(!showPassword);
        setIcon(showPassword ? 'eye-with-line' : 'eye')
    };

    const formValidation = () => {
        let newErrors = {};
        let validated = true

        if (login.username === '') {
            newErrors.username = 'Enter Username';
            validated = false;
        }
        if (login.password === '') {
            newErrors.password = 'Enter Password';
            validated = false;
        }
        setErrors(newErrors);
        return validated
    }

    async function loginUser() {
        API.login(login.username, login.password)
            .then(res => {
                // console.log(res)
                signIn(`${res.token_type} ${res.access_token}`)
            })
            .catch(err => {
                alert(err.message ? err.message : err);
                // console.log(err.message)
                // setLoadingProfile(false);
            })
    }

    const handleSubmit = () => {
        if (formValidation()) {
            loginUser()

        }
    }


    useEffect(() => {
        setErrors({})
    }, [])
    return (

        <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={{ backgroundColor: color.background, flex: 1 }}>

            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={{ flex: 1 }}>
                    <View style={{ paddingLeft: 20, backgroundColor: color.primary, flex: 1 }}>

                    </View>

                    <View style={{ backgroundColor: color.background, borderTopLeftRadius: 25, borderTopRightRadius: 25, flex: 1, marginTop: -25, paddingBottom: 50 }}>
                        <View style={{ position: 'absolute', top: -150, left: 20 }}>
                            <Image style={{ width: '70%', resizeMode: 'contain', alignSelf: 'flex-start' }} source={require(`../resource/logoSmall.png`)} />
                            <Text style={{ fontSize: 20, paddingBottom: 50, fontWeight: 'bold', color: color.secondaryText }}>
                                Digital Payment management
                            </Text>
                        </View>


                        <View style={styles.container}>

                            <Input
                                placeholder={'Username'}
                                label={'Username'}
                                labelStyle={{ paddingStart: 8, color: color.tetriatyText }}
                                inputContainerStyle={[styles.textInput, { borderBottomWidth: 0 }]}
                                containerStyle={styles.inputContainer}
                                onChangeText={value => setLogin({ username: value, password: login.password })}
                                errorMessage={errors.username}
                                onSubmitEditing={() => { this.secondTextInput.focus() }}
                                renderErrorMessage={false}
                                blurOnSubmit={false}
                                returnKeyType="next"
                                autoCapitalize="none"

                            />
                            <Input
                                ref={(input) => { this.secondTextInput = input; }}
                                autoCapitalize="none"
                                placeholder={'Password'}
                                label={'Password'}
                                labelStyle={{ paddingStart: 8, color: color.tetriatyText }}
                                inputContainerStyle={[styles.textInput, { borderBottomWidth: 0 }]}
                                rightIcon={<IconButton
                                    icon={<Icon type="entypo" name={passwordIcon} size={24} color={color.tetriatyText} />}
                                    onPress={toggleShowPassword} />}
                                containerStyle={[styles.inputContainer]}
                                secureTextEntry={!showPassword}
                                onChangeText={value => setLogin({ username: login.username, password: value })}
                                errorMessage={errors.password}
                                renderErrorMessage={false}

                            />

                            <Button
                                title="Login"
                                onPress={() => handleSubmit()} />
                        </View>

                    </View>
                </View>
            </TouchableWithoutFeedback>

        </KeyboardAvoidingView>

    )
}

export default Login