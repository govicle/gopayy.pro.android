import { View, Text, Image, FlatList, ScrollView, SectionList, StyleSheet, TouchableOpacity } from "react-native"
import { color } from "../style/color"
import styles from "../style/style"
import Card from "../components/card"
import Header from "../components/header"
import React from "react"
import { Avatar, Icon } from "react-native-elements"


const ViewAll = ({ route, navigation }) => {
    const { data,role } = route.params

    const handlePress = (item) => {
        navigation.navigate(`Summary`, {
            item: item,
            // orgID: responseHome.organizations[0].id
        })
        // console.log(`Pressed ${item.title}`);
        // Add your logic here
    };
    const Today = new Date()

    const ListItem = ({ item}) => {
        // console.log(role)
        // console.log(JSON.stringify(item))
        const newListTotal = [item.total_days[0], item.total_months[0]]
        return (
            <View style={{paddingEnd:8}}>
                <Card onPress={() => {
                    handlePress((role == `merchant`) ?
                        {
                            type: 'staff',
                            id: item.id,
                            title: item.user.name
                        }
                        :
                        {
                            type: 'merchant',
                            id: item.id,
                            title: item.name
                        }
                    )
                }} cardStyle={{ paddingEnd: 4 }}>
                    <View>
                        <View style={{ flexDirection: 'row', flexWrap: `wrap`, alignContent: `space-between` }}>
                            <View style={{ width: 60, height: 60, overflow: 'hidden', alignSelf: 'center', }}>
                                {(role == `merchant`) ?
                                    item.profile_picture ?
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            source={{
                                                uri:
                                                    item.profile_picture,
                                            }}
                                        /> :
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            titleStyle={{ fontWeight: `bold` }}
                                            title={item.user.name ? item.user.name.split(" ").map((n, index, array) => index === 0 || index === array.length - 1 ? n[0] : "").join("") : "WM"}
                                            containerStyle={{ backgroundColor: color.primary }}
                                        />
                                    :
                                    item.m_logo ?
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            source={{
                                                uri:
                                                    item.m_logo,
                                            }}
                                        /> :
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            titleStyle={{ fontWeight: `bold` }}
                                            title={item.user.name ? item.user.name.split(" ").map((n, index, array) => index === 0 || index === array.length - 1 ? n[0] : "").join("") : "WM"}
                                            containerStyle={{ backgroundColor: color.primary }}
                                        />
                                }



                            </View>
                            <View style={{ paddingStart: 16, paddingEnd: 8, flex: 1 }}>
                                <Text style={styles.HeaderTextMain}>{
                                    (role == `merchant`) ?
                                        item.user.name
                                        :
                                        item.name
                                }</Text>
                                <Text style={{ width: 150 }}>{(role == `merchant`) ?
                                    item.shift.status ?? `N/A`
                                    :
                                    item.m_address ?? `N/A`}</Text>

                            </View>
                            {/* <TouchableOpacity>
                            <View style={{ justifyContent: 'flex-start' }}>
                                <Icon type="antdesign"
                                    name="setting"
                                    size={20}
                                    color={color.primary} />
                            </View>
                        </TouchableOpacity> */}
                        </View>
                        <View style={{ backgroundColor: color.secondary, borderRadius: 14, flexDirection: `row`, alignItems: `center`, justifyContent: `center`, padding: 8, width: `100%` }}>
                            {(newListTotal) &&
                                newListTotal.map((day, index) =>
                                    <>
                                        <View style={{ alignItems: `center`, justifyContent: 'center', width: `45%` }}>
                                            <Text style={[styles.CardText, { fontSize: 12, color: `#A4A4A4`, fontWeight: 400 }]}>{day.title}</Text>
                                            <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, flexDirection: `row` }}>
                                                <Text style={{ color: color.primaryText, fontSize: 8 }}>RM</Text>
                                                <Text style={{ color: color.primaryText, fontWeight: `600`, fontSize: 18 }}>{(day.total) ? (parseFloat(day.total) > 1000) ? `${(parseFloat(day.total) / 1000).toFixed(1)}K~` : parseFloat(day.total).toFixed(2) : `0.00`}</Text>
                                            </View>
                                        </View >
                                        {
                                            (index != newListTotal.length - 1) &&
                                            <View style={{ width: 2, height: 30, backgroundColor: `#c8c8c8` }} />
                                        }
                                    </>

                                )
                            }
                        </View>
                    </View>

                </Card>
            </View>
        );
    };

    const ListProduct = ({ item }) => {
        const newListTotal = [item.total_days[0], item.total_months[0]]
        return (
            <View style={{ paddingEnd: 8 }}>
                <Card onPress={() => handlePress({
                    type: 'product',
                    id: item.id,
                    title: item.name
                })} cardStyle={{ padding: 0 }}>
                    <View style={{}}>
                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ width: 60, height: 60, borderRadius: 50, overflow: 'hidden', alignSelf: 'center' }}>
                                <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: item.logo }} />
                            </View>
                            <View style={{ paddingStart: 16, width: `80%`, alignItems: `center` }}>
                                <Text style={[styles.HeaderTextMain, { fontSize: 20, padding: 4 }]}>{item.name}</Text>
                                <View style={{ backgroundColor: color.secondary, borderRadius: 14, flexDirection: `row`, alignItems: `center`, justifyContent: `center`, padding: 8 }}>
                                    {(newListTotal) &&
                                        newListTotal.map((day, index) =>
                                            <>
                                                <View style={{ alignItems: `center`, justifyContent: 'center', width: `45%` }}>
                                                    <Text style={[styles.CardText, { fontSize: 12, color: `#A4A4A4`, fontWeight: 400 }]}>{day.title}</Text>
                                                    <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, flexDirection: `row` }}>
                                                        <Text style={{ color: color.primaryText, fontSize: 8 }}>RM</Text>
                                                        <Text style={{ color: color.primaryText, fontWeight: `600`, fontSize: 18 }}>{(day.total) ? (parseFloat(day.total) > 1000) ? `${(parseFloat(day.total) / 1000).toFixed(1)}K~` : parseFloat(day.total).toFixed(2) : `0.00`}</Text>
                                                    </View>
                                                </View >
                                                {
                                                    (index != newListTotal.length - 1) &&
                                                    <View style={{ width: 2, height: 30, backgroundColor: `#c8c8c8` }} />
                                                }
                                            </>

                                        )
                                    }
                                </View>
                            </View>
                            {/* <TouchableOpacity>
                                <View style={{ justifyContent: 'flex-start' }}>
                                    <Icon type="antdesign"
                                        name="setting"
                                        size={20}
                                        color={color.primary} />
                                </View>
                            </TouchableOpacity> */}
                        </View>

                    </View>
                </Card>
            </View>
        );
    };


    return (
        <View style={{ flex: 1 }}>
            <Header
                title={data.type}
                onBackPress={() => {
                    navigation.goBack()
                }}
            />

            <View style={{ flex: 1 }}>
                {
                    (data.logo) &&
                    <View style={{ flexDirection: `row`, marginEnd: 8, alignItems: 'center', paddingTop: 16, paddingBottom: 16 }}>

                        <View style={{ width: 35, height: 35, borderRadius: 25, overflow: 'hidden', marginStart: 16 }}>
                            <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: data.logo }} />
                        </View>



                        <Text style={[styles.HeaderTextSmall, { flex: 1, paddingStart: 8 }]}>{data.title}</Text>
                    </View>
                }
                <FlatList
                    data={data.data}
                    ListHeaderComponent={() => {

                    }}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        if (data.type == "Staff") {
                            return < ListItem item={item} />
                        } else {
                            return < ListProduct item={item} />
                        }
                    }}


                />
            </View>



        </View>
    )
}

export default ViewAll