import { View, Text, Image, FlatList, ScrollView, SectionList, StyleSheet, TouchableOpacity, ActivityIndicator, Animated } from "react-native"
import Button from "../components/button"
import { color } from "../style/color"
import styles from "../style/style"
import Card from "../components/card"
import Header from "../components/header"
import AuthContext from "../util/AuthContext"
import { API, getToken, removeToken } from "../util/services"
import React, { useEffect, useState, useRef } from "react"
import { Avatar, Icon } from "react-native-elements"
import { useIsFocused } from "@react-navigation/native"
import Loading from "../components/loading"
import receiptConfig from "../util/modelClass/receiptConfig"
import paymentChannels from "../util/modelClass/paymentChannel"
import moment from "moment";
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView, useSafeAreaInsets } from "react-native-safe-area-context"
import { LogLevel, OneSignal } from 'react-native-onesignal';
import * as Device from 'expo-device';
import * as Notifications from 'expo-notifications';
import Constants from "expo-constants";
// import Icon from "react-native-vector-icons/AntDesign"



const Home = ({ navigation }) => {
    const back = true
    const { signOut } = React.useContext(AuthContext)
    const isFocused = useIsFocused()
    const [loadingHome, setLoadingHome] = useState(false)
    const [responseHome, setResponseHome] = useState([])
    const [user, setUser] = useState([])
    const [section, setSection] = useState([])
    const insets = useSafeAreaInsets()

    const scrollY = new Animated.Value(0);
    const scrollWidth = new Animated.Value(0);

    const paddingInterpolate = scrollY.interpolate({
        inputRange: [0, 200], // Adjust these values based on your requirements
        outputRange: [24, 4], // Padding changes from 18 to 24
        extrapolate: 'clamp', // Prevent padding from going beyond these values
    });

    const sizeInterpolate = scrollWidth.interpolate({
        inputRange: [0, 50, 100], // Adjust these values based on your requirements
        outputRange: [50, 30, 25], // Padding changes from 18 to 24
        extrapolate: 'clamp', // Prevent padding from going beyond these values
    });


    const animatedStyle = {
        paddingHorizontal: paddingInterpolate,
        paddingTop: paddingInterpolate,
    };

    const animateIcon = {
        width: sizeInterpolate,
        height: sizeInterpolate,
    }


    useEffect(() => {
        if (isFocused) {
            registerForPushNotificationsAsync()
            OneSignal.Debug.setLogLevel(6);
            OneSignal.initialize(Constants.expoConfig.extra.oneSignalAppId);
            // Also need enable notifications to complete OneSignal setup
            OneSignal.Notifications.requestPermission(true);
            setLoadingHome(true)
            getHome()
           
        }

    }, [isFocused])

    async function registerForPushNotificationsAsync() {
        const appConfig = require('../app.json');
        const projectId = appConfig?.expo?.extra?.eas?.projectId;
        let token;

        if (Platform.OS === 'android') {
            await Notifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: Notifications.AndroidImportance.MAX,
                vibrationPattern: [0, 250, 250, 250],
                lightColor: '#FF231F7C',
            });
        }

        if (Device.isDevice) {
            const { status: existingStatus } = await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Notifications.requestPermissionsAsync();
                finalStatus = status;
            }
            token = (await Notifications.getExpoPushTokenAsync({ projectId: projectId })).data;
        } else {
            alert('Must use physical device for Push Notifications');
        }

       

        return token;
    };

    async function getHome() {

        getToken().then(token => {
            // console.log(token)
            API.getHome(token)
                .then(res => {
                    receiptConfig.setConfig(res.data.receipt_configs)
                    paymentChannels.setChannels(res.data.payment_channels)
                    // setLoadingHome(false)
                    setResponseHome(res.data)
                    assignUser(res.data)

                    // console.log(JSON.stringify(res))
                    // setJoblist(res.joblists)
                    // setUser(res.user)

                })
                .catch(err => {
                    setLoadingHome(false)
                    alert(err.message ? `Server Error : ${err.message}` : err);
                    // console.log(err.message)
                    setLoadingProfile(false);
                })
            API.info(token)
            .then(res => {
                if (res.success) {
                    // setUserInfo(res.user);
                    console.log(`${res.user.user_id}@gopayypro`)
                    OneSignal.login(`${res.user.user_id}@gopayypro`)
                    // OneSignal.User.addEmail(`${res.user.email}`)
                }
                else {
                    alert(res.message ?? res.error ?? res.errors ?? `System Error`);
                    
                }
            })
            .catch(err => {
                alert(err.message ?? err.error ?? err.errors ?? err);
                setLoadingProfile(false);
            })
        })
    }

    function assignUser(data) {
        switch (data.role) {
            case `merchant`:
                {
                    var orgProduct = []
                    data.organizations.forEach(org => {
                        var obj = { title: org.name, logo: org.logo, list: org.products }
                        orgProduct.push(obj)
                    })
                    var merchant = data.organizations[0].merchants[0]
                    var user = {
                        id: merchant.id,
                        name: merchant.name,
                        shortname: merchant.shortname,
                        phone: merchant.phone,
                        fax: merchant.fax,
                        email: merchant.email,
                        logo: merchant.m_logo,
                        address: merchant.m_address,
                        code: merchant.m_code,
                        status: merchant.status,
                        company_id: merchant.company_id,
                        total_months: merchant.total_months,
                        total_days: merchant.total_days,
                        child: merchant.staffs
                    }
                    setupSection({ title: 'Staff', list: merchant.staffs }, orgProduct)
                    setUser(user)
                    setLoadingHome(false)
                    break
                }

            case 'organization':
                {
                    var org = data.organizations[0]
                    var user = {
                        id: org.id,
                        name: org.name,
                        shortname: org.shortname,
                        phone: org.phone,
                        fax: org.fax,
                        email: org.email,
                        logo: org.logo,
                        address: org.address,
                        code: org.code,
                        status: org.status,
                        company_id: org.company_id,
                        total_months: org.total_months,
                        total_days: org.total_days,
                        child: org.merchants
                    }
                    setupSection({ title: 'Merchant', list: org.merchants }, [{ title: org.name, logo: org.logo, list: org.products }])
                    setUser(user)
                    setLoadingHome(false)
                    break
                }

        }


    }

    function setupSection(child, products) {
        var sectionData = [
            {
                title: child.title,
                type: 'Staff',
                horizontal: true,
                viewAll: child.list.length > 5 ? true : false,
                data: child.list,
            }]
        products.map((product) => {
            var newObj = {
                title: product.title,
                type: 'Product',
                viewAll: product.list.length > 5 ? true : false,
                logo: product.logo,
                data: product.list
            }
            sectionData.push(newObj)
        })
        // console.log(sectionData)
        setSection(sectionData)
    }

    const SECTIONS = (user) => [
        {
            title: 'Staff',
            type: 'Staff',
            horizontal: true,
            viewAll: true,
            data: user.child,
        },
        {
            title: 'Majlis Perbandaran Hang Tuah Jaya',
            type: 'Product',
            viewAll: true,
            logo: "https://gopayy.test.govicle.dev/storage/organization_logo/1/01Z9jXwNWb3oNljZITl1jgRGBS0wxgC1deg0nIUs.png",
            data: data2
        },
        // {
        //     title: 'Majlis Sepang',
        //     type: 'Product',
        //     viewAll: true,
        //     logo: "https://gopayy.test.govicle.dev/storage/organization_logo/5/A68TrVujvv90gCUjxHR71WgNJsW32iS6Csisl9QW.png",
        //     data: data2
        // },
    ];

    const Today = new Date()
    const DateFormated = Today.toLocaleString('default', { day: '2-digit' }) + ' ' + Today.toLocaleString('default', { month: 'long' }) + ' ' + Today.toLocaleString('default', { year: 'numeric' })

    const ListChild = ({ item }) => {
        const newListTotal = [item.total_days[0], item.total_months[0]]
        return (
            <View>
                <Card onPress={() => {
                    handlePress((responseHome.role == `merchant`) ?
                        {
                            type: 'staff',
                            id: item.id,
                            title: item.user.name
                        }
                        :
                        {
                            type: 'merchant',
                            id: item.id,
                            title: item.name
                        }
                    )
                }} cardStyle={{ paddingEnd: 4 }}>
                    <View>
                        <View style={{ flexDirection: 'row', flexWrap: `wrap`, alignContent: `space-between` }}>
                            <View style={{ width: 60, height: 60, overflow: 'hidden', alignSelf: 'center', }}>
                                {(responseHome.role == `merchant`) ?
                                    item.profile_picture ?
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            source={{
                                                uri:
                                                    item.profile_picture,
                                            }}
                                        /> :
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            titleStyle={{ fontWeight: `bold` }}
                                            title={item.user.name ? item.user.name.split(" ").map((n, index, array) => index === 0 || index === array.length - 1 ? n[0] : "").join("") : "WM"}
                                            containerStyle={{ backgroundColor: color.primary }}
                                        />
                                    :
                                    item.m_logo ?
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            source={{
                                                uri:
                                                    item.m_logo,
                                            }}
                                        /> :
                                        <Avatar
                                            rounded
                                            size={`medium`}
                                            titleStyle={{ fontWeight: `bold` }}
                                            title={item.user.name ? item.user.name.split(" ").map((n, index, array) => index === 0 || index === array.length - 1 ? n[0] : "").join("") : "WM"}
                                            containerStyle={{ backgroundColor: color.primary }}
                                        />
                                }



                            </View>
                            <View style={{ paddingStart: 16, paddingEnd: 8, flex: 1 }}>
                                <Text style={styles.HeaderTextMain}>{
                                    (responseHome.role == `merchant`) ?
                                        item.user.name
                                        :
                                        item.name
                                }</Text>
                                <Text style={{ width: 150 }}>{(responseHome.role == `merchant`) ?
                                    item.shift.status ?? `N/A`
                                    :
                                    item.m_address ?? `N/A`}</Text>

                            </View>
                            {/* <TouchableOpacity>
                            <View style={{ justifyContent: 'flex-start' }}>
                                <Icon type="antdesign"
                                    name="setting"
                                    size={20}
                                    color={color.primary} />
                            </View>
                        </TouchableOpacity> */}
                        </View>
                        <View style={{ backgroundColor: color.secondary, borderRadius: 14, flexDirection: `row`, alignItems: `center`, justifyContent: `center`, padding: 8, width: 250 }}>
                            {(newListTotal) &&
                                newListTotal.map((day, index) =>
                                    <>
                                        <View style={{ alignItems: `center`, justifyContent: 'center', width: `45%` }}>
                                            <Text style={[styles.CardText, { fontSize: 12, color: `#A4A4A4`, fontWeight: 400 }]}>{day.title}</Text>
                                            <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, flexDirection: `row` }}>
                                                <Text style={{ color: color.primaryText, fontSize: 8 }}>RM</Text>
                                                <Text style={{ color: color.primaryText, fontWeight: `600`, fontSize: 18 }}>{(day.total) ? (parseFloat(day.total) > 1000) ? `${(parseFloat(day.total) / 1000).toFixed(1)}K~` : parseFloat(day.total).toFixed(2) : `0.00`}</Text>
                                            </View>
                                        </View >
                                        {
                                            (index != newListTotal.length - 1) &&
                                            <View style={{ width: 2, height: 30, backgroundColor: `#c8c8c8` }} />
                                        }
                                    </>

                                )
                            }
                        </View>
                    </View>

                </Card>
            </View>
        );
    };


    const ListProduct = ({ item }) => {
        const newListTotal = [item.total_days[0], item.total_months[0]]
        return (
            <View style={{}}>
                <Card onPress={() => handlePress(
                    {
                    type: 'product',
                    id: item.id,
                    title: item.name},
                    
                )} cardStyle={{ padding: 0 }}>
                    <View style={{}}>
                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ width: 60, height: 60, borderRadius: 50, overflow: 'hidden', alignSelf: 'center' }}>
                                <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: item.logo }} />
                            </View>
                            <View style={{ paddingStart: 16, width: `80%`, alignItems: `center` }}>
                                <Text style={[styles.HeaderTextMain, { fontSize: 20, padding: 4 }]}>{item.name}</Text>
                                <View style={{ backgroundColor: color.secondary, borderRadius: 14, flexDirection: `row`, alignItems: `center`, justifyContent: `center`, padding: 8 }}>
                                    {(newListTotal) &&
                                        newListTotal.map((day, index) =>
                                            <>
                                                <View style={{ alignItems: `center`, justifyContent: 'center', width: `45%` }}>
                                                    <Text style={[styles.CardText, { fontSize: 12, color: `#A4A4A4`, fontWeight: 400 }]}>{day.title}</Text>
                                                    <View style={{ justifyContent: `center`, alignItems: `center`, borderRadius: 5, flexDirection: `row` }}>
                                                        <Text style={{ color: color.primaryText, fontSize: 8 }}>RM</Text>
                                                        <Text style={{ color: color.primaryText, fontWeight: `600`, fontSize: 18 }}>{(day.total) ? (parseFloat(day.total) > 1000) ? `${(parseFloat(day.total) / 1000).toFixed(1)}K~` : parseFloat(day.total).toFixed(2) : `0.00`}</Text>
                                                    </View>
                                                </View >
                                                {
                                                    (index != newListTotal.length - 1) &&
                                                    <View style={{ width: 2, height: 30, backgroundColor: `#c8c8c8` }} />
                                                }
                                            </>

                                        )
                                    }
                                </View>
                            </View>
                            {/* <TouchableOpacity>
                                <View style={{ justifyContent: 'flex-start' }}>
                                    <Icon type="antdesign"
                                        name="setting"
                                        size={20}
                                        color={color.primary} />
                                </View>
                            </TouchableOpacity> */}
                        </View>

                    </View>
                </Card>
            </View>
        );
    };

    const ListHeader = () => {
        return (
            <View style={{ paddingBottom: 0 }}>

                <View style={{ flexDirection: 'column', flexWrap: `wrap`, alignContent: `space-between`, backgroundColor: color.primary, paddingHorizontal: 24, paddingBottom: 40 }}>

                    <View style={{ flexDirection: `row`, flexWrap: `wrap`, alignItems: `center` }}>
                        {(user.total_days) &&
                            user.total_days.slice().reverse().map((item, index) =>
                                <>
                                    <View style={{ paddingHorizontal: 8, flex: 1, alignContent: 'center', justifyContent: 'center' }}>
                                        <Text style={[styles.CardText, { fontSize: 12, color: `#C8C8C8`, fontWeight: 400 }]}>{item.title}</Text>
                                        {/* <Text style={[styles.CardText, { fontSize: 15 }]}>{DateFormated}</Text> */}
                                        <View style={{ flexDirection: `row`, alignItems: `baseline`, justifyContent: `center` }}>
                                            <Text style={[styles.CardText, { fontSize: 12, padding: 0 }]}>RM</Text>
                                            <Text style={[styles.CardText, { padding: 0 }]}>{parseFloat(item.total).toFixed(2)}</Text>
                                        </View>
                                        <View style={{ margin: 8, borderRadius: 24, flex: 1, backgroundColor: (parseFloat(item.diff).toFixed(2) < 0) ? `#CE3D49` : `#53B0AE`, alignContent: 'center', justifyContent: 'center' }}>
                                            <Text style={[styles.CardText, { fontSize: 12, color: `white`, padding: 8, marginTop: 0 }]}>{(parseFloat(item.diff) < 0) ? parseFloat(item.diff).toFixed(2) : `+${parseFloat(item.diff).toFixed(2)}`}</Text>
                                        </View>

                                    </View >
                                    {
                                        (index != user.total_days.length - 1) &&
                                        <View style={{ width: 2, height: 70, backgroundColor: `#599690` }} />
                                    }
                                </>

                            )
                        }
                    </View>
                </View>

                <View style={{ marginTop: -40, flex: 1 }} >
                    <Card>
                        <View style={{ height: 30, backgroundColor: `white` }} >
                            <View style={{ flexDirection: `row`, alignItems: `center`, justifyContent: `center` }}>
                                {(user.total_months) &&
                                    user.total_months.slice().reverse().map((item, index) =>
                                        <>
                                            <View style={{ alignItems: `center`, paddingHorizontal: 16, flex: 1 }}>
                                                <Text> {moment.utc(item.date).local().format(`MMMM`)}</Text>
                                                <Text style={{ color: (parseFloat(item.diff).toFixed(2) < 0) ? `#CE3D49` : `#53B0AE` }}>{(parseFloat(item.diff).toFixed(2) < 0) ? parseFloat(item.diff).toFixed(2) : `+${parseFloat(item.diff).toFixed(2)}`} </Text>
                                            </View>
                                            {
                                                (index != user.total_months.length - 1) &&
                                                <View style={{ width: 1, height: 20, backgroundColor: `#599690` }} />
                                            }
                                        </>

                                    )
                                }

                            </View>
                        </View>
                    </Card>

                </View>


            </View>

        );
    }

    const handlePress = (item) => {
        navigation.navigate(`Summary`, {
            item: item,
            orgID: responseHome.organizations[0].id
        })
        // console.log(`Pressed ${item.title}`);
        // Add your logic here
    };

    return (
        <View style={{ flex: 1, backgroundColor: color.primary }}>



            <View style={{ backgroundColor: color.primary, paddingTop: insets.top }} />

            {(loadingHome) ?
                <Loading />
                :
                <LinearGradient
                    colors={[color.primary, color.primary, color.background, color.background]}
                    style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <Animated.View style={[animatedStyle, { flexDirection: 'column', flexWrap: `wrap`, alignContent: `space-between`, backgroundColor: color.primary, }]}>
                            <View style={{ flexDirection: `row`, flexWrap: `wrap`, alignItems: `center`, justifyContent: `space-between`, paddingHorizontal: 8, paddingTop: 16, paddingBottom: 8, width: `100%` }}>

                                <View style={{ flexDirection: `row`, alignItems: `center`, justifyContent: `flex-start`, paddingHorizontal: 16 }}>

                                    <Animated.View style={[
                                        { borderRadius: 50, overflow: 'hidden' },
                                        animateIcon
                                    ]}>
                                        <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: user.logo }} />
                                    </Animated.View>
                                    <View style={{ paddingStart: 8, paddingEnd: 8 }}>
                                        <Text style={[styles.HeaderTextMain, { color: `white` }]}>{user.name}</Text>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        removeToken()
                                        signOut()
                                        OneSignal.logout
                                    }}
                                    style={{}}
                                >
                                    <View style={{ alignSelf: `center` }}>
                                        <Icon type="material-icons"
                                            name="logout"
                                            size={24}
                                            color={`white`} />
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </Animated.View>

                        <SectionList
                            onRefresh={() => {
                                setLoadingHome(true)
                                getHome()
                            }
                            }
                            refreshing={loadingHome}
                            contentContainerStyle={{ backgroundColor: color.background }}
                            stickySectionHeadersEnabled={false}
                            sections={section}

                            onScroll={Animated.event(
                                [{ nativeEvent: { contentOffset: { y: scrollY } } }],
                                {
                                    useNativeDriver: false,
                                    listener: (event) => {
                                        const y = event.nativeEvent.contentOffset.y;
                                        scrollWidth.setValue(y);
                                    }
                                }
                            )}
                            scrollEventThrottle={1}


                            ListHeaderComponent={ListHeader}
                            renderSectionHeader={({ section }) => (
                                <>
                                    <View style={{ flexDirection: `row`, marginEnd: 8, alignItems: 'center', paddingTop: 16 }}>
                                        {
                                            (section.logo) &&
                                            <View style={{ width: 35, height: 35, borderRadius: 25, overflow: 'hidden', marginStart: 16 }}>
                                                <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: section.logo }} />
                                            </View>
                                        }


                                        <Text style={[styles.HeaderTextSmall, { flex: 1, paddingStart: 8 }]}>{section.title}</Text>
                                        {section.viewAll ? (
                                            <TouchableOpacity
                                                onPress={() => navigation.navigate(`view all`, { data: section, role: responseHome.role })}>
                                                <Text style={{ color: color.primary, paddingEnd: 16 }}>View All</Text>
                                            </TouchableOpacity>
                                        ) : null
                                        }
                                    </View>
                                    {section.horizontal ? (
                                        <FlatList
                                            horizontal
                                            data={section.data}
                                            renderItem={({ item }) => <ListChild item={item} />}
                                            showsHorizontalScrollIndicator={false}
                                        />
                                    ) : null}
                                </>
                            )}
                            renderItem={({ item, section }) => {
                                if (section.horizontal) {
                                    return null;
                                }
                                return <ListProduct item={item} />;
                            }}
                        />
                    </View>
                </LinearGradient>
            }


        </View>



    )


}

export default Home