import { View, Text, Image, FlatList, ScrollView, SectionList, StyleSheet, TouchableOpacity, Modal } from "react-native"
import Button from "../components/button"
import { useEffect, useState } from "react";
import { color } from "../style/color"
import styles from "../style/style"
import Card from "../components/card"
import Header from "../components/header"
import AuthContext from "../util/AuthContext"
import { API, getToken, removeToken } from "../util/services"
import React from "react"
import { Icon } from "react-native-elements"
import AccordionView from "../components/accordion";
import Collapsible from "react-native-collapsible";
import Loading from "../components/loading";
import { useIsFocused } from "@react-navigation/native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import moment from "moment";
import receiptConfig from "../util/modelClass/receiptConfig";
import ImageViewer from "react-native-image-zoom-viewer";



const Receipt = ({ route, navigation }) => {
    const { receipt, config } = route.params
    const isFocused = useIsFocused()
    const [collectionBy, setCollectionBy] = useState(`monthly`)
    const [channels, setChannels] = useState()
    const [loading, setLoading] = useState(false)
    const [organizationId, setOrganizationId] = useState()
    const [logs, setLogs] = useState([])
    const [datePickerMode, setDatePickerMode] = useState(`date`)
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [isYearPickerVisible, setIsYearPickerVisible] = useState(false);
    const [isCollectionByPickerVisible, setIsCollectionByPickerVisible] = useState(false);
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState()
    const currentYear = new Date().getFullYear();
    const [skeleton, setSkeleton] = useState([])
    const [cart, setCart] = useState([])
    const [cartItem, setCartItem] = useState([])
    const [cartItemCount, setCartItemCount] = useState([])
    const [imageView, openImageView] = useState({ open: false, index: 0, urls: [] })

    useEffect(() => {
        if (isFocused) {
            // setLoading(true)
            setSkeleton(JSON.parse(receipt.skeleton))
            setCart(JSON.parse(receipt.cart_json))
            processSubItem(JSON.parse(receipt.cart_json))
            // processSubItem(JSON.parse(receipt.cart_json).subItem)
            // console.log(JSON.stringify(receipt))
            // console.log(JSON.stringify(config))
        }

    }, [isFocused])

    const handlePress = (item) => {
        // console.log(`Pressed ${JSON.stringify(item)}`);
        // Add your logic here
    };

    const statusText = {
        '2': `APPROVED`,
        '4': `SETTLED`,
        '5': `VOIDED`,
        'others': `SETTLED VOID`
    }

    function processSubItem(subitem) {
        var listObj = []
        if (subitem != null) {
            subitem.forEach(sub => {
                var list = []
                var obj = sub
                while (obj.subItem != null) {
                    list.push(obj.subItem)
                    obj = obj.subItem

                }
                // console.log(obj)
                listObj.push(list)
            })
            setCartItem(listObj)
        }


    }


    return (

        <View style={{ flex: 1 }}>
            <Modal visible={imageView.open} transparent={true} onRequestClose={() => openImageView({ open: false, index: 0, urls: [] })}>
                <ImageViewer
                    imageUrls={imageView.urls}
                    // index={imageView.index}
                    enableSwipeDown={true}
                    failImageSource={config.logo_path}
                    onCancel={() => openImageView({ open: false, index: 0, url: [] })} />
            </Modal>
            <Header
                title={`Receipt`}
                onBackPress={() => {
                    navigation.goBack()
                }}
            />
            {
                // Flags in config:
                // "show_logo"
                // "show_merchant_name"
                // "show_text_header"
                // "show_text_footer"
                // "show_staff_name"
                // "show_staff_code"
                // "show_subtotal"
                // "show_tax"
                // "show_total"
                // "show_rounding"
                // "show_net_total"
                (loading) ?
                    <Loading />
                    :
                    <ScrollView>
                          <View style={{paddingHorizontal: 16, paddingVertical: 16,alignItems: `center` }}>
                                    <Text style= {[styles.HeaderTextBlack,{fontSize:24}]}>{statusText[receipt.status]}</Text>
                                </View>
                        <Card>
                            <View style={{ alignItems: `center` }}>

                                {(config.show_logo) &&
                                    <View style={{ width: `100%`, height: 150, padding: 8, overflow: 'hidden' }}>
                                        <Image style={{ width: '100%', height: '100%', resizeMode: `contain` }} source={{ uri: config.logo_path }} />
                                    </View>
                                }
                                {
                                    (config.show_merchant_name) &&
                                    <Text style={{ padding: 8 }}>{receipt.merchant.name}</Text>
                                }
                                {
                                    (config.show_text_header) &&
                                    <Text style={{ padding: 4 }}>{config.text_header}</Text>
                                }

                                <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                    <Text style={{ flex: 1 }}>Receipt No</Text>
                                    <Text>{receipt.receipt_number}</Text>
                                </View>

                                <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                    <Text style={{ flex: 1 }}>Date</Text>
                                    <Text>{moment.utc(receipt.paid_at).local().format(`DD/MM/YYYY`)}</Text>
                                </View>

                                <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                    <Text style={{ flex: 1 }}>Time</Text>
                                    <Text>{moment.utc(receipt.paid_at).local().format(`hh:mm:ss a`)}</Text>
                                </View>

                                {(!(config.show_staff_name == false && config.show_staff_code == false)) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>Cashier</Text>
                                        {
                                            (config.show_staff_code && config.show_staff_name) ?
                                                <Text>{receipt.staff.user.name}{(receipt.staff.staff_code)&& ` (${receipt.staff.staff_code})`}</Text>
                                                : (config.show_staff_code) ?
                                                    <Text>{receipt.staff.staff_code}</Text>
                                                    : <Text>{receipt.staff.user.name}</Text>

                                        }
                                    </View>
                                }

                                <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                    <Text style={{ flex: 1 }}>Channel</Text>
                                    <Text>{receipt.channel.name}</Text>
                                </View>

                                {
                                    (receipt.bill_no) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>Bill No</Text>
                                        <Text>{receipt.bill_no}</Text>
                                    </View>
                                }
                                <View style={{ width: `100%` }}>
                                    <FlatList
                                        data={skeleton}
                                        scrollEnabled={false}
                                        keyExtractor={(item, index) => index}
                                        renderItem={({ item }) => <>
                                            {(item.value) &&
                                                <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                                    <Text style={{ flex: 1 }}>{item.skeletonTitle}</Text>
                                                    <Text>{item.value}</Text>
                                                </View>}
                                        </>}
                                    />
                                </View>


                                <View style={{ width: `95%`, borderBottomWidth: 1, paddingHorizontal: 16, paddingVertical: 4, borderBottomColor: color.tetriatyText }} />
                                {
                                    (cart) &&
                                    <>

                                        <View style={{ width: `100%` }}>
                                            <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                                <Text style={{ width: `60%` }}>Item</Text>
                                                <Text style={{ width: `20%`, textAlign: `center` }}>Qty</Text>
                                                <Text style={{ width: `20%`, textAlign: `right` }} >Total</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: `100%` }}>
                                            <FlatList
                                                data={cart}
                                                scrollEnabled={false}
                                                keyExtractor={(item, index) => index}
                                                renderItem={({ item, index }) => <>
                                                    {
                                                        (item.value) &&
                                                        <>
                                                            <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                                                <Text style={{ width: `60%` }}>{item.value}</Text>
                                                                <Text style={{ width: `20%`, textAlign: `center` }}>{cartItem[index][cartItem[index].length - 1].quantity}</Text>
                                                                <Text style={{ width: `20%`, textAlign: `right` }} >{parseFloat(cartItem[index][cartItem[index].length - 1].totalAmount).toFixed(2)}</Text>
                                                            </View>
                                                            <View style={{ paddingHorizontal: 32, paddingVertical: 4 }}>
                                                                {
                                                                    cartItem[index].map((item, index) =>
                                                                        <View style={{ flexDirection: `row`, paddingHorizontal: (16 * index), paddingVertical: 4 }}>
                                                                            <Text > -{item.value}</Text>
                                                                        </View>
                                                                    )

                                                                }
                                                            </View>


                                                        </>

                                                    }
                                                </>}
                                            />
                                        </View>


                                        <View style={{ width: `95%`, borderBottomWidth: 1, paddingHorizontal: 16, paddingVertical: 4, borderBottomColor: color.tetriatyText }} />

                                    </>
                                }
                                {(config.show_subtotal) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>Sub Total</Text>
                                        <Text>{parseFloat(receipt.subtotal).toFixed(2)}</Text>
                                    </View>
                                }

                                {(config.show_tax) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>SST({receipt.tax_rate}%)</Text>
                                        <Text>{parseFloat(receipt.tax_value).toFixed(2)}</Text>
                                    </View>
                                }

                                {(config.show_total) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>Total</Text>
                                        <Text>{parseFloat(receipt.total).toFixed(2)}</Text>
                                    </View>
                                }

                                {(config.show_rounding) &&
                                    <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 4 }}>
                                        <Text style={{ flex: 1 }}>Rounding</Text>
                                        <Text>{parseFloat(receipt.rounding).toFixed(2)}</Text>
                                    </View>
                                }

                                {(config.show_subtotal) &&
                                    <>
                                        <View style={{ width: `95%`, borderBottomWidth: 1, paddingHorizontal: 16, paddingVertical: 4, borderBottomColor: color.tetriatyText }} />

                                        <View style={{ flexDirection: `row`, paddingHorizontal: 16, paddingVertical: 16 }}>
                                            <Text style={{ flex: 1 }}>Net Total</Text>
                                            <Text>{parseFloat(receipt.net_total).toFixed(2)}</Text>
                                        </View>
                                    </>

                                }

                                <View style={{ width: `95%`, borderBottomWidth: 1, paddingHorizontal: 16, paddingVertical: 4, borderBottomColor: color.tetriatyText }} />
                                {
                                    (config.show_text_footer) &&
                                    <Text style={{ padding: 16 }}>{config.text_footer}</Text>
                                }

                            </View>
                        </Card>

                        {(!(receipt.image_proof == null && receipt.image_receipt == null)) &&
                            <>

                                <View style={{ flexDirection: `row`, padding: 16 }}>
                                    {(receipt.image_proof) &&
                                        <View>
                                            <Text style={[{}]} > Proof Photo</Text>
                                            <TouchableOpacity onPress={() => openImageView({ open: true, index: 1, urls: [{ url: receipt.image_proof }] })}>
                                                <View style={{ width: 150, height: 200, margin: 4, borderRadius: 14, overflow: 'hidden' }}>
                                                    <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: receipt.image_proof }} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>

                                    }
                                    {(receipt.image_receipt) &&
                                        <View>
                                            <Text style={[{}]} > Receipt Photo</Text>
                                            <TouchableOpacity onPress={() => openImageView({ open: true, index: 1, urls: [{ url: receipt.image_receipt }] })}>
                                                <View style={{ width: 150, height: 200, margin: 4, borderRadius: 14, overflow: 'hidden' }}>
                                                    <Image style={{ width: '100%', height: '100%', resizeMode: 'cover' }} source={{ uri: receipt.image_receipt }} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </View>


                            </>

                        }


                    </ScrollView>


            }

        </View >


    )


}
export default Receipt