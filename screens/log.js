import { View, Text, Image, FlatList, ScrollView, SectionList, StyleSheet, TouchableOpacity, Modal, ActivityIndicator } from "react-native"
import Button from "../components/button"
import { useEffect, useState } from "react";
import { color } from "../style/color"
import styles from "../style/style"
import Card from "../components/card"
import Header from "../components/header"
import AuthContext from "../util/AuthContext"
import { API, getToken, removeToken } from "../util/services"
import React from "react"
import { Icon } from "react-native-elements"
import AccordionView from "../components/accordion";
import Collapsible from "react-native-collapsible";
import Loading from "../components/loading";
import { useIsFocused } from "@react-navigation/native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DatePicker, { getFormatedDate } from 'react-native-modern-datepicker';
import moment from "moment";
import receiptConfig from "../util/modelClass/receiptConfig";


const Log = ({ route, navigation }) => {
    const { filters } = route.params
    const isFocused = useIsFocused()
    const [collectionBy, setCollectionBy] = useState(`monthly`)
    const [channels, setChannels] = useState()
    const [loading, setLoading] = useState(false)
    const [loadMore, setLoadMore] = useState(false)
    const [organizationId, setOrganizationId] = useState()
    const [logs, setLogs] = useState([])
    const [datePickerMode, setDatePickerMode] = useState(`date`)
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [isYearPickerVisible, setIsYearPickerVisible] = useState(false);
    const [isCollectionByPickerVisible, setIsCollectionByPickerVisible] = useState(false);
    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState()
    const [toDateStart, setToDateStart] = useState(true)
    const currentYear = new Date().getFullYear();
    const [currentPage,setCurentPage] = useState()
    const [totalPage,setTotalPage] = useState()

    useEffect(() => {
        if (isFocused) {
            setLoading(true)

            getLog(filters)
            // console.log(filters)

        }

    }, [isFocused])
    async function getLog(filtersItem) {
        getToken().then(token => {
            var merchantId = ``
            var productId = ``
            var staffId = ``
            var organizationIs = ``
            switch (filtersItem.user.type) {
                case `merchant`: {
                    merchantId = filtersItem.user.id
                    break
                }
                case `staff`: {
                    staffId = filtersItem.user.id
                    break
                }
                case `product`: {
                    productId = filtersItem.user.id
                    break
                }
                default: {
                    break
                }
            }
            API.getLog(token, filtersItem.channels, organizationId, merchantId, staffId, productId, moment(filtersItem.date_start).format(`YYYY-MM-DD`), filtersItem.date_end ? moment(filtersItem.date_end).format(`YYYY-MM-DD`) : null)
                .then(res => {
                    setLoading(false)
                    // console.log(JSON.stringify(res))
                    setLogs(res.data.logs.data)
                    setCurentPage(parseInt(res.data.logs.current_page))
                    setTotalPage(parseInt(res.data.logs.last_page))
                    // console.log(`current page: ${res.data.logs.current_page}  last page: ${res.data.logs.last_page}`)
                    // setJoblist(res.joblists)
                    // setUser(res.user)

                })
                .catch(err => {
                    setLoading(false)
                    alert(err.message ? err.message : err);
                    // console.log(err.message)
                    // setLoadingProfile(false);
                })
        })
    }

    async function getLogpages(page) {
        getToken().then(token => {
            var merchantId = ``
            var productId = ``
            var staffId = ``
            var organizationIs = ``
            switch (filters.user.type) {
                case `merchant`: {
                    merchantId = filters.user.id
                    break
                }
                case `staff`: {
                    staffId = filters.user.id
                    break
                }
                case `product`: {
                    productId = filters.user.id
                    break
                }
                default: {
                    break
                }
            }
            API.getLog(token, filters.channels, organizationId, merchantId, staffId, productId, moment(filters.date_start).format(`YYYY-MM-DD`), filters.date_end ? moment(filters.date_end).format(`YYYY-MM-DD`) : null,page)
                .then(res => {
                    setLoadMore(false)
                    // console.log(JSON.stringify(res))
                    let currentLogs = logs
                    var data = res.data.logs.data
                    data.forEach(element => {
                        currentLogs.push(element)
                    });
                    
                    setLogs(currentLogs)
                    // console.log(JSON.stringify(res))
                    // setLogs(res.data.logs.data)
                    setCurentPage(parseInt(res.data.logs.current_page))
                    setTotalPage(parseInt(res.data.logs.last_page))
                    // console.log(`current page: ${res.data.logs.current_page}  last page: ${res.data.logs.last_page}`)
                    // setJoblist(res.joblists)
                    // setUser(res.user)

                })
                .catch(err => {
                    setLoadMore(false)
                    alert(err.message ? err.message : err);
                    // console.log(err.message)
                    // setLoadingProfile(false);
                })
        })
    }

    const statusColors = {
        '2': `#6CD5DA`,
        '4': `#41D013`,
        '5': `#FFA842`,
        'others': `#FF5C5C`
    }
    const statusIcons = {
        '2': `check`,
        '4': `check`,
        '5': `close`,
        'others': `close`
    }


    const handlePress = (item) => {
        // console.log(`Pressed ${JSON.stringify(item)}`);
        // console.log(`config ${JSON.stringify(receiptConfig.getConfig(item.item.product_id))}`)
        navigation.navigate(`Receipt`, { receipt: item.item, config: receiptConfig.getConfig(item.item.product_id) })

        // Add your logic here
    };

    const [isCollapsed, setIsCollapse] = useState([])

    function getDateString(date) {
        switch (filters.collection_by) {
            case `yearly`:
                return date.toLocaleString("en-MY", { year: `numeric`, month: `long` });
            default:
                return date.toLocaleString("en-MY", { year: `numeric`, month: `short`, day: `2-digit` });
        }

    }

    const LogItem = ({ item }) => {

        return (

            <View>
                <Card onPress={() => handlePress({ item })} >
                    <View style={{ alignContent: `space-between` }}>

                        <View style={{ flexDirection: `row`, flexWrap: `wrap`, alignContent: `space-between`, alignItems: "center" }}>
                            <View style={{ flex: 1, justifyContent: `center` }}>
                                <Text style={styles.HeaderTextMain}>{item.product.name}</Text>
                                <Text style={{ color: color.tetriatyText, fontSize: 14 }}>{item.receipt_number}</Text>
                                <Text style={{ color: color.tetriatyText, fontSize: 14 }}>{item.staff.user.name}</Text>
                            </View>
                            <View style={{ alignItems: `flex-end`, justifyContent: `flex-end` }}>
                                <Text style={[styles.HeaderTextBlack, { color: `black` }]}>RM{parseFloat(item.total).toFixed(2)}</Text>
                                <Text style={{ color: color.tetriatyText, fontSize: 14 }}>{moment.utc(item.paid_at).local().format(`YYYY/MM/DD hh:mm:ss a`)}</Text>
                                <View style={{ flexDirection: `row`, alignContent: `space-between` }}>
                                    <View style={{ paddingHorizontal: 8, justifyContent: `center`, backgroundColor: color.primarySoft, borderRadius: 10, marginTop: 4, marginHorizontal: 4 }}>
                                        <Text style={{ color: color.secondaryText, fontSize: 10, paddingVertical: 8 }}>{item.channel.name}</Text>
                                    </View>
                                    <View style={{ paddingHorizontal: 8, justifyContent: `center`, backgroundColor: statusColors[item.status], borderRadius: 10, marginTop: 4 }}>
                                        <Icon type="font-awsome" name={statusIcons[item.status]} size={17} color={color.secondary} />
                                    </View>
                                </View>

                            </View>
                        </View>

                        <View style={{ backgroundColor: color.secondary, borderRadius: 14, justifyContent: 'center' }}>
                        </View>
                    </View>
                </Card>
            </View>

        );
    };


    return (
        <View style={{ flex: 1 }}>
            <Header
                title={`Log`}
                onBackPress={() => {
                    navigation.goBack()
                }}
            />
            {
                (loading) ?
                    <Loading />
                    :
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>
                            <FlatList
                                ListHeaderComponent={
                                    <View>
                                        <View style={{ flexDirection: `row`, marginVertical: 8, marginHorizontal: 16, alignItems: `center` }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    // setToDateStart(true)
                                                    // showDatePicker()
                                                }}
                                                style={{ flex: 1, marginVertical: 8, marginEnd: 8, borderRadius: 10, backgroundColor: `white`, alignItems: `center`, justifyContent: `center`, paddingHorizontal: 8, paddingVertical: 16 }}>
                                                <Text style={[styles.HeaderTextBlack]}>{getDateString(new Date(filters.date_start))}</Text>
                                            </TouchableOpacity>
                                            {
                                                (collectionBy == `daterange`) &&
                                                <>
                                                    <Text style={styles.HeaderTextBlack}>-</Text>

                                                    <TouchableOpacity
                                                        onPress={() => {
                                                            setToDateStart(false)
                                                            showDatePicker()

                                                        }}
                                                        style={{ flex: 1, marginVertical: 8, marginStart: 8, borderRadius: 10, backgroundColor: `white`, alignItems: `center`, justifyContent: `center`, paddingHorizontal: 8, paddingVertical: 16 }}>
                                                        <Text style={[styles.HeaderTextBlack]}>{moment(filters.date_end).format(`DD MMMM YYYY`)}</Text>
                                                    </TouchableOpacity>
                                                </>
                                            }



                                        </View>
                                        {
                                            (logs.length == 0) &&
                                            <View style={{ flex: 1, alignItems: `center`, justifyContent: `center` }}>
                                                <Image style={{ resizeMode: 'contain' }} source={require(`../resource/empty.png`)} />
                                            </View>
                                        }

                                    </View>}
                                data={logs}
                                onEndReached={() => {
                                    if(currentPage != totalPage){
                                        setLoadMore(true)
                                        getLogpages(currentPage+1)
                                    }
                                 }}
                                onEndReachedThreshold={0}
                                ListFooterComponent={
                                    (loadMore)&&
                                    <View style={{ marginTop: 10, alignItems: "center" }}>
                                        <ActivityIndicator size="large" color="#1f1f1f" />
                                    </View>
                                }
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => `${Math.random(5)}${item.id}${item.product_id}`}
                                renderItem={({ item }) => < LogItem item={item} />
                                }
                            />
                        </View>


                    </View>



            }

        </View>


    )


}
export default Log