const color = {

    primary : "#026F64",
    primarySoft : "#1C9A8E",
    secondary : "#F4F9FE",
    background: "#F2F2F2",

    primaryText : "#26363E",
    secondaryText : "#FFFFFF",
    tetriatyText :"#616161"

}

export {color}