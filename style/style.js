import { StyleSheet } from "react-native";
import { color } from "./color";

const styles = StyleSheet.create({

    container : {
        paddingRight: 16,
        paddingLeft : 16,
        paddingTop:24,
        paddingBottom:8,
    },

    inputContainer : {
        marginBottom:20,
    },

    textInput : {
        backgroundColor : color.secondary,
        color : color.primaryText,
        borderRadius: 14,
        paddingRight: 14,
        paddingLeft : 14,
        paddingTop:8,
        paddingBottom:8,
    },

    text : {
        marginStart:8,
        padding : 4,
        color:color.primaryText
    },

    textSecondary : {
        marginStart:8,
        padding : 4,
        color:color.secondaryText
    },

    HeaderText : {
        marginStart:8,
        marginTop:4,
        padding : 4,
        color:color.secondaryText,
        fontSize : 20,
        fontWeight : '900',
        textAlignVertical:`center`
    },

    HeaderTextSmall : {
        marginStart:8,
        marginTop:4,
        padding : 4,
        color:color.primaryText,
        fontSize : 15,
        fontWeight : 'bold',
        textAlignVertical:`center`
    },

    CardText : {
        marginTop:4,
        padding : 4,
        color:color.background,
        fontSize : 18,
        fontWeight : 'bold',
        textAlignVertical:`center`,
        textAlign:'center'
    },

    CardTextSecondary : {
        marginTop:4,
        padding : 4,
        color:color.primaryText,
        fontSize : 15,
        fontWeight : 'bold',
        textAlignVertical:`center`,
        textAlign:'center'
    },

    HeaderTextMain : {
        color:color.primary,
        fontSize : 20,
        fontWeight : 'bold'
    },

    HeaderTextBlack : {
        color:color.tetriatyText,
        fontSize : 16,
        fontWeight : 'bold'
    },

    inputTitleText : {
        marginStart:16,
        padding : 4,
        paddingTop:8,
        color:color.tetriatyText,
        fontSize:14,
        fontWeight:"thin"
    },

    buttonMain : {
        backgroundColor : color.primary,
        color : color.primaryText,
        borderRadius: 50,
        margin:8,
        marginTop:16,
        alignItems:"center",
        padding : 16
    },

    buttonText : {
        color:color.secondaryText,
        fontSize:18,
        fontWeight:"bold"
    },
    card: {
        backgroundColor: 'white',
        borderRadius: 14,
        width: '100%',
        padding:16,
      }



})

export default styles