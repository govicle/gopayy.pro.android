class ReceiptConfig{
    constructor(){
        if(!ReceiptConfig.instance){
            ReceiptConfig.instance = this;
            this.data = null;
        }
        return ReceiptConfig.instance;
    }
    setConfig(configs){
        this.data = configs
    }

    getConfig(productId){
        return this.data.find(item => item.product_id == productId)
    }

    getConfigs(){
        return this.data
    }
}

const receiptConfig = new ReceiptConfig();

export default receiptConfig;