class PaymentChannels{
    constructor(){
        if(!PaymentChannels.instance){
            PaymentChannels.instance = this;
            this.data = null;
        }
        return PaymentChannels.instance;
    }
    setChannels(channels){
        this.data = channels
    }

    getChannels(){
        return this.data.filter(data => data.code != `invoice`)
    }
}

const paymentChannels = new PaymentChannels();

export default paymentChannels;
