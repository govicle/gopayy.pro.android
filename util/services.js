import AsyncStorage from '@react-native-async-storage/async-storage';


export async function setToken(value) {
    await AsyncStorage.setItem("token_gopayypro", value)
}

export async function getToken() {
    return await AsyncStorage.getItem("token_gopayypro")
}

export async function removeToken() {
    await AsyncStorage.setItem("token_gopayypro", '')
}

//stag ENV
export const ENV = {
    service_user: `https://user.test.govicle.dev`,
    service_gopayy: `https://gopayy.test.govicle.dev`,
    clientId: `92152dae-6f43-4b92-8aa6-2e848dc45f07`,
    clientSecret: `5DtXvi3hY0NIVP5UIk8Ue9pab7Y5q2EL8TeaBNZO`,
    grantType: `password`
}

//prod ENV
// export const ENV = {
//     service_user: `https://user.govicle.com`,
//     service_gopayy: `https://gopayy.govicle.com`,
//     clientId: `9250683d-0e7b-4af8-b061-e300a92a3d2c`,
//     clientSecret: `a4yOSv1KcPbrE3lt9G5byqo77jE3fmZDzr96QW7V`,
//     grantType: `password`
// }

export const API = {
    async login(username, password) {
        const url = `${ENV.service_user}/oauth/token`;
        // console.log(`username : ${username} password : ${password}`)
        return await fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                client_id: ENV.clientId,
                client_secret: ENV.clientSecret,
                grant_type: ENV.grantType,
                username: username,
                password: password
            }),
        })
            .then(async (response) => {
                // console.log(`test : ${JSON.stringify(response.json)}`)
                let json = response.json();
                if (response.status != 200) {
                    // console.log('error at userLogin()')
                    return json.then(Promise.reject.bind(Promise));
                }
                else {

                    return json;
                }
            })
    },
    async info(userToken) {
        const url = `${ENV.service_user}/api/info`;
        // console.log(`username : ${username} password : ${password}`)
        return await fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: userToken
            },
        })
            .then(async (response) => {
                // console.log(`test : ${JSON.stringify(response.json)}`)
                let json = response.json();
                if (response.status != 200) {
                    // console.log('error at userLogin()')
                    return json.then(Promise.reject.bind(Promise));
                }
                else {

                    return json;
                }
            })
    },
    async getHome(userToken) {
        const url = `${ENV.service_gopayy}/api/pro`;
        // console.log(`username : ${username} password : ${password}`)
        return await fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: userToken
            }
        })
            .then(async (response) => {
                // console.log(`test : ${JSON.stringify(response.json)}`)
                let json = response.json();
                if (response.status != 200) {
                    // console.log('error at getHome()')
                    return json.then(Promise.reject.bind(Promise));
                }
                else {

                    return json;
                }
            })
    },
    async getSummary(userToken, collectionBy, channels, organizationID, merchantID,staffID, productID,dateStart,dateEnd) {
        var param = `?collection_by=${collectionBy}`
        channels && (param += `&channels=${channels.toString()}`)
        organizationID && (param += `&organization=${organizationID}`)
        merchantID && (param += `&merchant=${merchantID}`)
        staffID && (param += `&staff=${staffID}`)
        productID && (param += `&product=${productID}`)
        dateStart && (param += `&date_start=${dateStart}`)
        dateEnd && (param += `&date_end=${dateEnd}`)

        const url = `${ENV.service_gopayy}/api/pro/summary${param}`;
        console.log(`Request : ${url}`)
        return await fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: userToken
            }
        })
            .then(async (response) => {
                // console.log(`test : ${JSON.stringify(response.json)}`)
                let json = response.json();
                if (response.status != 200) {
                    // console.log('error at getPaymentSummary()')
                    return json.then(Promise.reject.bind(Promise));
                }
                else {

                    return json;
                }
            })
    },
    async getLog(userToken, channels, organizationID, merchantID, staffID, productID,dateStart,dateEnd,page) {
        var param = `?`
        channels && (param += `&channels=${channels.toString()}`)
        organizationID && (param += `&organization=${organizationID}`)
        merchantID && (param += `&merchant=${merchantID}`)
        staffID && (param += `&staff=${staffID}`)
        productID && (param += `&product=${productID}`)
        dateStart && (param += `&date_start=${dateStart}`)
        dateEnd && (param += `&date_end=${dateEnd}`)
        page && (param += `&page=${page}`)

        const url = `${ENV.service_gopayy}/api/pro/log${param}`;
        // console.log(`Request : ${url}`)
        return await fetch(url, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: userToken
            }
        })
            .then(async (response) => {
                // console.log(`test : ${JSON.stringify(response.json)}`)
                let json = response.json();
                if (response.status != 200) {
                    // console.log('error at getPAymentChannel()')
                    return json.then(Promise.reject.bind(Promise));
                }
                else {

                    return json;
                }
            })
    },
    
}