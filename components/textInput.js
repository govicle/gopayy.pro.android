import { View,Text} from "react-native";
import styles from "../style/style";
import { Input } from "react-native-elements";

const TextInputGrow = ({title,onInput,errorMessage}) => {
    return(
        <View>
            <Text style ={styles.inputTitleText}>
                {title}
            </Text>
            <Input
                placeholder = {title}
                label ={title}
                style={styles.textInput}
                inputContainerStyle = {styles.inputContainer}
                onChangeText = {onInput}
                errorMessage= {errorMessage}
                renderErrorMessage={false}

            />
        </View>
        
    )
}

export default TextInputGrow