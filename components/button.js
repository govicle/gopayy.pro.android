import { View,Text, TouchableOpacity} from "react-native";
import styles from "../style/style";

const Button = ({title,onPress}) => {
    return(
        <View>
            <TouchableOpacity 
                style = {styles.buttonMain}
                onPress= {onPress} >

            <Text style ={styles.buttonText}>
                {title}
            </Text>
            </TouchableOpacity>
        </View>
        
    )
}

export default Button