import { Text, Image, View  } from "react-native"
import { SafeAreaView, useSafeAreaInsets } from "react-native-safe-area-context"
import { color } from "../style/color"
import styles from "../style/style"
import { TouchableOpacity } from "react-native-gesture-handler"
import { Icon } from "react-native-elements"

const Header = ({ title, logo, onBackPress, onLogoutPress }) => {
    const insets = useSafeAreaInsets()
    return (
        <View style={{ backgroundColor: color.primary, flexDirection: `row`, height: 'auto', alignItems: 'center', paddingStart: 20, paddingEnd: 20,paddingBottom:16,paddingTop:insets.top+20}}>
            <View style={{flex:1, flexDirection:'row',alignItems: 'center'}}>
                {
                    (onBackPress) &&
                    <TouchableOpacity onPress={onBackPress}>
                        <Icon type="antdesign" name="leftcircleo" size={24} color={color.secondary} />
                    </TouchableOpacity>
                }
                {
                    (logo) &&
                    <Image style={{ width:140,height:40, resizeMode:'contain'}} source={logo} />
                }

                <Text style={styles.HeaderText}>{title}</Text>
            </View>

            {
                (onLogoutPress) &&
                <TouchableOpacity onPress={onLogoutPress} style={{ marginLeft: 'auto',justifyContent:'center' }}>
                    <Icon type="feather" name="log-out" size={24} color={color.secondary} />
                </TouchableOpacity>
            }

        </View>
    )
}

export default Header