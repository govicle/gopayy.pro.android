import { Text, Image, View  } from "react-native"
import { SafeAreaView, useSafeAreaInsets } from "react-native-safe-area-context"
import { color } from "../style/color"
import styles from "../style/style"
import { TouchableOpacity } from "react-native-gesture-handler"
import { Icon } from "react-native-elements"
import LottieView from 'lottie-react-native'
import { useRef } from "react"

const Loading = ({ title, logo, onBackPress, onLogoutPress }) => {
    const animation = useRef (null);
    return (
        <View style={{ flex: 1, alignItems: `center`, justifyContent: `center` }}>
        <LottieView
            autoPlay
            ref={animation}
            style={{
                width: 200,
                height: 200,
            }}
            // Find more Lottie files at https://lottiefiles.com/featured
            source={require('../resource/loading.json')}
        />
    </View>
    )
}

export default Loading