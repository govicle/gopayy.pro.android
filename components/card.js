import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import { Shadow } from "react-native-shadow-2";
import { color } from '../style/color';

const Card = ({ children, onPress, cardColor, cardStyle }) => {
  // const shadowStyle = {
  //   shadowOffset: { width: 1, height: 4 },
  //   shadowOpacity: 0.05,
  //   shadowRadius: 0.05,
  //   elevation: 1,
  //   backgroundColor: color.primary, // Background color must be set when using shadow
  //   borderRadius: 14,
  //   width: "100%",
  // };

  return (
    <>
      {
        (onPress) ?
          <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
            <View style={[styles.container,cardStyle]}>
              {/* <Shadow style={shadowStyle}> */}
                <View style={[styles.innerContainer,styles.shadowPreset]}>{children}</View>
              {/* </Shadow> */}
            </View>
          </TouchableOpacity>
          :
          <View style={[styles.container,cardStyle]}>
            {/* <Shadow style={shadowStyle}> */}
              <View style={[styles.innerContainer,styles.shadowPreset]}>{children}</View>
            {/* </Shadow> */}
          </View>
      }
    </>

  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 14,
    paddingHorizontal:16,
    paddingVertical:8,
    overflow:`hidden`
   
  },
  innerContainer: {
    backgroundColor: `white`,
    padding: 16,
    borderRadius: 14,
  },
  square: {
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 4,
    height: 150,
    shadowColor: 'black',
    width: 150,
  },
  shadowPreset: {
    shadowColor: `#c8c8c8`, 
    shadowOffset: { width: 0, height: 1 }, 
    shadowOpacity: 0.5, 
    shadowRadius: 1, 
    elevation: 2
},
shadowExtra:{
    shadowColor:`#c8c8c8`, 
    shadowOffset: { width: 0, height: 0 }, 
    shadowOpacity: 0.5, 
    shadowRadius: 8, 
    elevation: 2,
}
});

export default Card;
