

import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import { Shadow } from "react-native-shadow-2";
import { color } from '../style/color';

const IconButton = ({ onPress, icon }) => {


    return (
        <TouchableOpacity onPress={onPress} style={{ flex: 0, justifyContent: 'center' }}>
            {icon}
        </TouchableOpacity>



    );
};

const styles = StyleSheet.create({
    container: {
        borderRadius: 14,
        padding: 16,
        overflow: 'hidden', // Ensure inner shadow doesn't overflow
    },
    innerContainer: {
        backgroundColor: `white`,
        padding: 16,
        borderRadius: 14,
    },
});

export default IconButton;
