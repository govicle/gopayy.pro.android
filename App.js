import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "./screens/login";
import Home from "./screens/home";
import * as Font from "expo-font"
import { useEffect, useState,useMemo,useReducer } from "react";
import { getToken,setToken } from "./util/services";
import AuthContext from "./util/AuthContext";
import ViewAll from "./screens/viewAll";
import Summary from "./screens/summary";
import Log from "./screens/log";
import Receipt from "./screens/receipt";


const Stack = createStackNavigator()
export default function App({ navigation }) {

    const [state, dispatch] = useReducer(
        (prevState, action) => {
          switch (action.type) {
            case 'RESTORE_TOKEN':
              return {
                ...prevState,
                userToken: action.token,
              };
            case 'SIGN_IN':
              return {
                ...prevState,
                isSignout: false,
                userToken: action.token
              };
            case 'SIGN_OUT':
              return {
                ...prevState,
                isSignout: true,
                userToken: null
              };
          }
        }, {
        isSignout: false,
        userToken: null
      }
      )

    useEffect(()=>{
        getToken().then(token => 
            {
            if (token != null && token != '') {
            dispatch({ type: 'RESTORE_TOKEN', token: token })
          }})
    },[])

    const authContext = useMemo(
        () => ({
          signIn: async (data) => {
            setToken(data)
              .then(() => {
                dispatch({ type: 'SIGN_IN', token: data });
              })
          },
          signOut: () => dispatch({ type: 'SIGN_OUT' }),
        }),
        []
      );

    return (
        <AuthContext.Provider value={authContext}>
        <NavigationContainer>
            <Stack.Navigator>
                {
                    (state.userToken == null) ?
                        <Stack.Group screenOptions={{ headerShown: false }} >
                            <Stack.Screen name="Login" component={Login} />
                        </Stack.Group>
                        : 
                        <Stack.Group screenOptions={{ headerShown: false }}>
                            <Stack.Screen name = "Home" component={Home} />
                            <Stack.Screen name = "view all" component={ViewAll} />
                            <Stack.Screen name = "Summary" component={Summary}/>
                            <Stack.Screen name = "Log" component={Log}/>
                            <Stack.Screen name = "Receipt" component={Receipt}/>
                           
                        </Stack.Group>
                }
            </Stack.Navigator>
        </NavigationContainer>
        </AuthContext.Provider>
    )
}